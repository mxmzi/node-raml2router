var express = require("express");
var _ = require("underscore");
var bodyParser = require('body-parser');

function makeContentTypeHandler(handlerMap) {
    return function(req, res, next) {
        var n = 0;
        var contentType = (req.get("content-type") || "")
            .replace(/;.*/, "")
            .toLowerCase();
        var handlers = handlerMap[contentType] || handlerMap["*/*"];
        if (!handlers) {
            return next("400 UNSUPPORTED CONTENT TYPE");
        }
        console.log(handlers);

        function handle(req, res) {
            var handler = handlers[n++];
            if (handler) {
                return handler(req, res, function(err) {
                    if (err) {
                        return next(err);
                    } else {
                        return handle(req, res);
                    }
                });
            } else {
                next();
            }
        }
        return handle(req, res);
    };
}

function makeSchemaValidatorHandler(schema, schemaValidator) {
    return function(req, res, next) {
        console.log("validate schema ", req.body, schema);
        schemaValidator(req.body, schema, function(err) {
            if (err) return next(err);
            else next();
        });
    };
}


var tv4Validate = function(data, schema, callback) {
    var result = require("tv4").validateResult(data, schema);
    callback((result.error || result.missing.length) ? result : null);
}


function raml2router(args, callback) {
    if (!args.raml) {
        if (args.ramlPath) {
            return require('raml-parser')
                .loadFile(args.ramlPath)
                .then(function(data) {
                    var arg = {
                        raml: data
                    };
                    return raml2router(_.chain(args)
                        .clone()
                        .extend(arg)
                        .value(), callback);
                }, function(error) {
                    return callback(error);
                });
        } else {
            callback("args.raml not defined");
        }
    }
    try {
        var raml = args.raml;
        var config = args.config;
        var handlerModule = args.handlerModule;
        var jValidator = args.jSchemaValidator || tv4Validate;
        var router = new express.Router();
        router.get("/_raml", function(req, res, next) {
            res.send(raml);
        });

        function expandHandlers(handlers, mimeType) {
            return handlers.map(function(h) {
                if (typeof h === 'object') {
                    h = h[mimeType];
                    console.log(mimeType, "---> ", h);
                    if (!h) {
                        console.log("no " + mimeType);
                        throw ("missing handler for type " + mimeType);
                    }
                }
                if (typeof h === 'function' || typeof h == 'string') {
                    h = [h];
                }
                if (Array.isArray(h)) {
                    return _.map(h, function(v) {
                        if (typeof v === 'string') {
                            v = handlerModule[v] || v;
                        }
                        if (typeof v !== 'function') {
                            throw ("Invalid handler: " + v);
                        }
                        return v;
                    });
                } else {
                    throw ("handler config error: " + typeof h);
                }
            });
        }

        _.each(raml.resources, function(r) {
            var path = r.relativeUri.replace(/\{/g, ":")
                .replace(/\}/g, "");
            console.log(path);
            _.each(r.methods, function(m) {
                var activeHandlers =
                    _.chain(config.handlers)
                    .pick(
                        _.union(r.is || [], m.is || [])
                    )
                    .filter(_.negate(_.isUndefined))
                    .value();
                //                    .map( getHander )
                //                    .filter( _.negate(_.isUndefined) )
                //                    .value();
                console.log(activeHandlers);
                console.log(m.method);
                var handlerMap = {};
                if (m.body) {
                    _.each(m.body, function(v, k) {
                        console.log(k, "################ ", v);
                        var handlers = expandHandlers(activeHandlers, k);
                        if (v && k.match(/^application\/(json|.+\+json)$/) && v.schema) {
                            console.log(v.schema);
                            handlers = [
                                function(req, res, next) {
                                    req.headers["content-type"] = "application/json";
                                    next();
                                },
                                bodyParser.json(),
                                makeSchemaValidatorHandler(JSON.parse(v.schema), jValidator),
                                handlers
                            ];
                        }
                        handlerMap[k] = _.flatten(handlers);
                    });
                } else {
                    handlerMap["*/*"] = _.flatten(expandHandlers(activeHandlers, "*/*"));
                }
                console.log("----------", m.method, " -- ", path, "\n", handlerMap);
                router[m.method](path, makeContentTypeHandler(handlerMap));
            });
            router.get("/_raml" + path, function(req, res, next) {
                res.send(r);
            });
            router.options(path, function(req, res, next) {
                res.send(r);
            });
        });

    } catch (e) {
        console.log("!!!!!!!!!!!1", e);
        return callback(e);
    }
    console.log("ok");
    return callback(null, router);
}





exports.makeRouter = raml2router;