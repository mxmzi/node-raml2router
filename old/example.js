   function authenticateHandler(req, res, next) {
       console.log("authenticate");
       next();
   }

   function getFileHandler(req, res, next) {
       console.log("getFile");
       res.send('getFile' + "\n");
   }

   function putFileBinaryHandler(req, res, next) {
       console.log("putFileBinary");
       res.send('putFileBinary' + "\n");
   }

   function putFileJsonHandler(req, res, next) {
       console.log("putFileJson");
       res.send('putFileJson' + "\n");
   }

   function slurpFileHandler(req, res, next) {
       console.log("slurpFile" + "\n");
       next();
   }

   var raml2router = require("./raml2router");
   var raml = require('raml-parser');
   var express = require('express');
   //   raml.loadFile(__dirname + '/example.raml').then(function (data) {

   //    console.log( JSON.stringify(data, null, ' ') );
   var config = {
       handlers: {
           authenticated: authenticateHandler,
           putFile: {
               "*/*": [slurpFileHandler, putFileBinaryHandler],
               "application/json": putFileJsonHandler,
               "application/vnd.fileref+json": "putFileJsonHandler"
           },
           getFile: getFileHandler
       }
   };
   console.log("makeRouter");

   raml2router.makeRouter({
       ramlPath: __dirname + '/example.raml',
       config: config,
       handlerModule: {
           putFileJsonHandler: putFileJsonHandler
       }
   }, function (err, router) {

       console.log(err, router);
       try {
           var app = express()

           app.use('/', router);

           console.log("router loaded");

           app.use(function (err, req, res, next) {
               var status = 500;
               if (typeof err === 'string') {
                   status = parseInt(err) || 500;
               }
               res.status(status)
                   .send({
                       error: err
                   });
           });



           var server = app.listen(3000, function () {

               var host = server.address()
                   .address;
               var port = server.address()
                   .port;

               console.log('Example app listening at http://%s:%s', host, port)

           });

       } catch (e) {
           console.log(e);
       }
   });

   //   }, function (error) {

   //       console.log('Error parsing: ' + error);

   //   });