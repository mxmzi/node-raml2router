var ramlRoutes = require("../../lib/raml-routes");
var ramlRouter = require("../../lib/raml-express-router");
var express = require("express");

function authenticateHandler(req, res, next) {
    console.log("authenticate");
    next();
}

function getFileHandler(req, res, next) {
    console.log("getFile");
    res.send('getFile' + "\n");
}

function putFileBinaryHandler(req, res, next) {
    console.log("putFileBinary");
    res.send('putFileBinary' + "\n");
}

function putFileJsonHandler(req, res, next) {
    console.log("putFileJson");
    res.send('putFileJson' + "\n");
}

function slurpFileHandler(req, res, next) {
    console.log("slurpFile" + "\n");
    next();
}

var contentTypeRules = {
    "*/*": {
        authenticated: authenticateHandler,
        putFile: [slurpFileHandler, putFileBinaryHandler],
        getFile: getFileHandler
    },
    "application/json": {
        putFile: putFileJsonHandler
    },
    "application/vnd.fileref+json": {
        putFile: putFileJsonHandler
    }
};

function jsonSchema(schema) {
    return function (req, res, next) {
        console.log("validating", schema);
        next();
    }
}


ramlRoutes(__dirname + '/example.raml', {}, function (err, routes) {

    try {
        console.log(">>>", JSON.stringify(routes, null, ' '));

        var config = {
            jsonSchemaValidator: jsonSchema
        };
        ramlRouter(routes, contentTypeRules, config, function (err, router) {
            try {
                var app = express()
                app.use('/', router);
                console.log("router loaded");
                app.use(function (err, req, res, next) {
                    var status = 500;
                    if (typeof err === 'string') {
                        status = parseInt(err) || 500;
                    }
                    res.status(status)
                        .send({
                            error: err
                        });
                });

                var server = app.listen(3000, function () {

                    var host = server.address()
                        .address;
                    var port = server.address()
                        .port;

                    console.log('Example app listening at http://%s:%s', host, port)

                });

            } catch (e) {
                console.log(e);
            }

        });


    } catch (e) {
        console.log(e);
    }

});