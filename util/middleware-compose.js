function middlewareComposeRecursive(mws, n) {
    'use strict';
    if (n === mws.length - 1) {
        return mws[n];
    } else {
        var nextMw = middlewareComposeRecursive(mws, n + 1),
            thisMw = mws[n];
        return function(req, res, next) {
            return thisMw(req, res, function(err) {
                if (err) {
                    return next(err);
                } else {
                    return nextMw(req, res, next);
                }
            });
        };
    }
}

function middlewareCompose(mws) {
    'use strict';
    if (mws.length === 0) {
        throw "Array is empty";
    }
    return middlewareComposeRecursive(mws, 0);
}

module.exports = middlewareCompose;