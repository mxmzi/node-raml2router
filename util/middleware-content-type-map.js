function middlewareContentTypeMap(mwMap) {
    return function(req, res, next) {
        var n = 0;
        var contentType = (req.get("content-type") || "")
            .replace(/;.*/, "")
            .toLowerCase();
        var mw = mwMap[contentType] || mwMap["*/*"];
        if (!mw) {
            return next("400 UNSUPPORTED CONTENT TYPE");
        }
        return mw(req, res, next);
    }
}

module.exports = middlewareContentTypeMap;