var ramlRoutes = require("./raml-routes");
var mwCompose = require("../util/middleware-compose");
var mwMap = require("../util/middleware-content-type-map");
var _ = require("underscore");
var express = require("express");
var bodyParser = require('body-parser');

function resolveRule(rules, type, trait) {
    return rules[type][trait] || rules["*/*"][trait]
}

function ramlExpressRouter(ramlRoutes, rules, config, cb) {
    var router = new express.Router();
    var log = ramlExpressRouter.log || console.log.bind(console);
    try {
        _.each(ramlRoutes, function (r) {
            var map = {};
            var body = r.body || {
                "*/*": null
            };
            _.each(body, function (v, k) {
                var mw = [];
                if (k.match(/^application\/(json|.+\+json)$/)) {
                    mw = [function (req, res, next) {
                            req.headers["content-type"] = "application/json";
                            next();
                        },
                        bodyParser.json()
                    ];
                    if (config.jsonSchemaValidator && v) {
                        mw.push(config.jsonSchemaValidator(JSON.parse(v)));
                    }
                }
                mw = mw.concat(_.chain(r.traits).map(_.partial(resolveRule, rules, k)).flatten().filter(_.negate(_.isUndefined))
                    .value());
                log(r.method, r.path, k, mw);
                if (mw.length === 0) {
                    mw.push(function (req, res, next) {
                        res.end("Nothing interesting here\n");
                    });
                }
                map[k] = mwCompose(mw);
            });
            router[r.method](r.path, mwMap(map));
        });
    } catch (e) {
        console.log(e);
        return cb(e);
    }
    return cb(null, router);
}

module.exports = ramlExpressRouter;