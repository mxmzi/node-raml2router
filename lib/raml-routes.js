var _ = require("underscore");

function ramlRoutes(raml, config, callback) {
    if (typeof raml !== 'object') {
        return require('raml-parser')
            .loadFile(raml)
            .then(function (data) {
                return ramlRoutes(data, config, callback);
            }, function (error) {
                return callback(error);
            });
    }
    var routes = [];
    try {
        _.each(raml.resources, function (r) {
            var path = r.relativeUri.replace(/\{/g, ":")
                .replace(/\}/g, "");
            console.log(path);
            _.each(r.methods, function (m) {
                var traits = _.union(r.is || [], m.is || []);
                console.log("traits", traits);
                var schemas = null;
                console.log(m.method);
                if (m.body) {
                    schemas = {};
                    _.each(m.body, function (v, k) {
                        console.log(k, "################ ", v);
                        schemas[k] = v ? v.schema || null : null;
                    });
                }

                routes.push({
                    method: m.method,
                    traits: traits,
                    path: path,
                    body: schemas
                });
            });

            routes.push({
                method: "options",
                traits: [],
                path: path,
                body: null
            });
        });

    } catch (e) {
        console.log("!!!!!!!!!!!1", e);
        return callback(e);
    }
    console.log("ok", routes);
    return callback(null, routes);
}


module.exports = ramlRoutes;